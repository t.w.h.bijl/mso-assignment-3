﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab3
{
    class PaymentAuthorizationService
    {
        public bool Authorize(string tID)
        {
            MessageBox.Show("Connecting to the Payment Authorization Service...");
            MessageBox.Show("Authorizing payment with transaction id [" + tID + "]");
            MessageBox.Show("Payment authorized");
            return true;
        }

    }
}
