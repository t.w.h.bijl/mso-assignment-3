﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    class Payment
    {
        public UIPayment payMethod;
        Ticket ticket;
        public bool completed;
        public string transactionId;

        public Payment(UIPayment pm, Ticket t)
        {
            this.ticket = t;
            payMethod = pm;
            SelectPayment();
        }

        public bool SelectPayment()
        {
            switch (payMethod)
            {
                case UIPayment.Cash:
                    CashPayment cp = new CashPayment(ticket);
                    completed = cp.HandlePayment();
                    transactionId = "N/A";
                    break;
                case UIPayment.CreditCard:
                    CardPayment credP = new CardPayment(UIPayment.CreditCard, ticket);
                    completed = credP.HandlePayment();
                    transactionId = credP.TransactionID;
                    break;
                case UIPayment.DebitCard:
                    CardPayment debP = new CardPayment(UIPayment.DebitCard, ticket);
                    completed = debP.HandlePayment();
                    transactionId = debP.TransactionID;
                    break;
                default:
                    throw new Exception("Invalid payment method");
            }
            return completed;
        }
        
       
    }
}
