﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab3
{
    class CashPayment
    {
        Ticket ticket;

        public CashPayment(Ticket t)
        {
            ticket = t;
        }

        public bool HandlePayment()
        {
            //In this case payments always work but can be changed to be able to get confirmed by machine.
            Starta();
            Betala();
            Stoppa();
            return true;
        }

        void Starta()
        {
            MessageBox.Show("Välkommen till IKEA Mynt Ätare 2000");
        }

        void Stoppa()
        {
            MessageBox.Show("Hejdå!");
        }

        void Betala()
        {
            MessageBox.Show(ticket.Price * 100 + " cent");
        }

    }
}
