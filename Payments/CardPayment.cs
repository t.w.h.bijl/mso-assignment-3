﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab3
{
    class CardPayment
    {
        bool creditCard;
        Ticket ticket;
        string transactionid;

        public CardPayment(UIPayment pm, Ticket t)
        {
            this.ticket = t;
            creditCard = (pm == UIPayment.CreditCard);
            transactionid = getTid();
        }

        public bool HandlePayment()
        {
            Connect();
            BeginTransaction();

            PaymentAuthorizationService pas = new PaymentAuthorizationService();
            bool comp = pas.Authorize(transactionid);

            Disconnect();

            return comp;

        }
        //https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings
        //Generates a random Transaction ID
        private string getTid()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var stringChars = new char[12];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            return new String(stringChars);
        }

        void Connect()
        {
            MessageBox.Show("Connecting to the card reader");
        }

        void Disconnect()
        {
            MessageBox.Show("Disconnecting from the card reader");
        }

        void BeginTransaction()
        {
            float pr = ticket.Price;
            if (creditCard)
                pr += 0.50f;
            MessageBox.Show("Starting transaction of €" + pr + "0");
        }

        public string TransactionID
        { get
            { return transactionid; }
        }

    }
}
