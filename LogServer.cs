﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Reflection;

namespace Lab3
{
    class LogServer
    {
        public void AddLog(Ticket t, Payment p)
        {
            Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, @"Logs\")); //Creates a folder for the logs if it doesn't already exist
            string fileName = DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss"); //Names the log after the current time
            string path = Path.Combine(Environment.CurrentDirectory, @"Logs\", fileName + ".txt"); //It's saved in the brand new Logs folder
            File.Create(path).Dispose();
            TextWriter tw = new StreamWriter(path);
            tw.WriteLine("---TICKET---");
            LogProperties(tw, t);
            tw.WriteLine();
            tw.WriteLine("---PAYMENT---");
            LogProperties(tw, p);
            tw.Close();
            MessageBox.Show("Interaction Logged");
        }

        private void LogProperties(TextWriter tw, object obj)
        {
            //https://stackoverflow.com/questions/6536163/how-to-list-all-variables-of-class

            //Puts the names of the fields in an array
            var fieldNames = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Select(field => field.Name).ToArray();

            //Puts the values of the fields in an array
            var fieldValues = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Select(field => field.GetValue(obj)).ToArray();

            //Prints the field names and values
            for (int z = 0; z < fieldValues.Length; z++) tw.WriteLine(fieldNames[z] + ": " + fieldValues[z]);
        }

    }
}
