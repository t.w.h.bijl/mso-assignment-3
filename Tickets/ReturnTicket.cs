﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    class ReturnTicket : Ticket
    {
        string firstStation;
        string secondStation;

        public ReturnTicket(string fStation, string sStation, UIClass trainClass, UIDiscount rd, DateTime dt)
        {
            firstStation = fStation;
            secondStation = sStation;
            railCard = rd;
            travelClass = trainClass;
            date = dt;
        }
    }
}
