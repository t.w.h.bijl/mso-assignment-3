﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    class SingleTicket : Ticket
    {
        string departureStation;
        string destinationStation;

        public SingleTicket(string depSt, string destSt, UIClass trainClass, UIDiscount rd, DateTime dt)
        {
            departureStation = depSt;
            destinationStation = destSt;
            railCard = rd;
            travelClass = trainClass;
            date = dt;
        }
    }
}
